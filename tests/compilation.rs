use assert_cmd::prelude::*;
use predicates::prelude::*;
use std::fs;
use std::path::Path;
use std::process::Command;

#[test]
fn no_input_file() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("ver")?;

    cmd.assert()
        .failure()
        .stderr(predicate::str::contains("no input files"));

    Ok(())
}

#[test]
fn non_existent_file() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("ver")?;

    cmd.arg("file/does/not/exist.ver")
        .assert()
        .failure()
        .stderr(predicate::str::contains("does not exist"));

    Ok(())
}

#[test]
fn not_ver_file() -> Result<(), Box<dyn std::error::Error>> {
    let mut cmd = Command::cargo_bin("ver")?;

    cmd.arg("file.txt")
        .assert()
        .failure()
        .stderr(predicate::str::contains(
            "input file must have .ver extension",
        ));

    Ok(())
}

#[test]
fn run_valid_file_tests() -> Result<(), Box<dyn std::error::Error>> {
    for dir in fs::read_dir(Path::new("./tests/valid/"))? {
        let dir = dir?;
        let path = dir.path();
        for entry in fs::read_dir(path)?
            .filter(|e| e.as_ref().unwrap().path().extension().unwrap() == "ver")
        {
            let entry = entry?;
            let path = entry.path();
            let expected = fs::read_to_string(&path.with_extension("txt"))?;
            let expected_pred = predicate::str::diff(expected.clone());
            let executable = Path::new("./").join(path.file_stem().unwrap());
            Command::cargo_bin("ver")?.arg(&path).assert().success();
            let cmd = Command::new(&executable).output()?;
            fs::remove_file(&executable)?;
            let actual = format!(
                "STDOUT={}\nSTDERR={}\nRETURNCODE={}\n",
                std::str::from_utf8(&cmd.stdout)?,
                std::str::from_utf8(&cmd.stderr)?,
                cmd.status.code().unwrap()
            );
            if !expected_pred.eval(&actual) {
                panic!("expected: {}\ngot: {}\nfor file: {}", &expected, actual, path.display());
            }
        }
    }
    Ok(())
}

#[test]
fn run_invalid_file_tests() -> Result<(), Box<dyn std::error::Error>> {
    for dir in fs::read_dir(Path::new("./tests/invalid/"))? {
        let dir = dir?;
        let path = dir.path();
        for entry in fs::read_dir(path)?
            .filter(|e| e.as_ref().unwrap().path().extension().unwrap() == "ver")
        {
            let entry = entry?;
            let path = entry.path();
            let expected = fs::read_to_string(&path.with_extension("txt"))?;
            let expected_pred = predicate::str::diff(expected.clone());
            let cmd = Command::cargo_bin("ver")?.arg(&path).output()?;
            let actual = format!(
                "STDOUT={}\nSTDERR={}\nRETURNCODE={}\n",
                std::str::from_utf8(&cmd.stdout)?,
                std::str::from_utf8(&cmd.stderr)?,
                cmd.status.code().unwrap()
            );
            if !expected_pred.eval(&actual) {
                panic!("expected: {}\ngot: {}\nfor file: {}", &expected, actual, path.display());
            }
        }
    }
    Ok(())
}
