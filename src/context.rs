use std::collections::HashMap;

pub struct Context {
    var_map: HashMap<String, i64>,
    stack_index: i64,
    label_counter: i32,
}

impl Context {
    pub fn new() -> Context {
        Context {
            var_map: HashMap::new(),
            stack_index: 0,
            label_counter: 0,
        }
    }

    pub fn add_var(&mut self, name: &String) {
        if let Some(_) = self.var_map.get(name) {
            panic!("ERROR: variable `{}` has already been declared", name);
        }
        self.stack_index -= 8; // Subtract 8 bytes because we push an entire 64-bit register
        self.var_map.insert(name.to_string(), self.stack_index);
    }

    pub fn get_var_offset(&self, name: &String) -> i64 {
        match self.var_map.get(name) {
            Some(off) => *off,
            None => panic!("ERROR: undeclared variable `{}`", name),
        }
    }

    pub fn generate_label(&mut self) -> String {
        let num = self.label_counter;
        self.label_counter += 1;
        format!("_LBL{:03}", num)
    }
}
