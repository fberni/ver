use crate::context::*;
use crate::parser::*;

pub fn generate(prog: &Program) -> String {
    let mut context = Context::new();
    format!(
        concat!(
            ".globl _start\n",
            ".text\n",
            "_start:\n",
            "callq main\n",
            "movq %rax, %rdi\n",
            "movq $60, %rax\n",
            "syscall\n\n",
            "{}"
        ),
        generate_function(&mut context, &prog.function)
    )
}

fn generate_function(context: &mut Context, function: &FnDecl) -> String {
    format!(
        concat!(
            ".globl {func_name}\n",
            ".text\n",
            "{func_name}:\n",
            "push %rbp\n",
            "movq %rsp, %rbp\n",
            "{func_body}\n"
        ),
        func_name = function.name,
        func_body = function
            .stmts
            .iter()
            .map(|stmt| generate_statement(context, stmt))
            .reduce(|a, b| { format!("{}\n{}", a, b) })
            .unwrap_or_default(),
    )
}

// TODO: simplify
fn generate_statement(context: &mut Context, statement: &Stmt) -> String {
    match statement {
        Stmt::Return(expression) => {
            format!(
                concat!("{}\n",
			"movq %rbp, %rsp\n",
			"pop %rbp\n",
			"\nretq"),
                generate_expression(context, &expression)
            )
        }
        Stmt::Expr(expr) => generate_expression(context, expr),
        Stmt::VarDecl(var, m_expr) => {
            let gen = format!(
                concat!("{}\n", "push %rax"),
                // Initialize variable from expression (zero init if no expression was provided)
                match m_expr {
                    Some(expr) => generate_expression(context, &expr),
                    None => "movl $0, %eax".to_string(),
                }
            );
            context.add_var(var);
            gen
        }
        Stmt::VarAssign(var, expr) => {
            format!(
                concat!("{}\n", "movq %rax, {}(%rbp)",),
                generate_expression(context, expr),
                context.get_var_offset(var)
            )
        }
    }
}

fn generate_expression(context: &mut Context, expression: &Expr) -> String {
    match expression {
        Expr::Constant(value) => format!("movl ${}, %eax", value),
        Expr::UnOp(op, expr) => format!(
            "{expr}\n{op}",
            op = match op {
                UnOp::Neg => "neg %eax",
                UnOp::BitComp => "not %eax",
                UnOp::Not => concat!("cmpl $0, %eax\n",
				     "movl $0, %eax\n",
				     "sete %al\n"),
            },
            expr = generate_expression(context, &*expr)
        ),
	Expr::BinOp(BinOp::And, lhs, rhs) =>
	    format!(
                concat!(
                    "{lhs}\n",
                    "cmpl $0, %eax\n",
                    "jne {rhs_lbl}\n",
                    "jmp {end_lbl}\n",
                    "{rhs_lbl}:\n",
                    "{rhs}\n",
                    "cmpl $0, %eax\n",
                    "mov $0, %eax\n",
                    "setne %al\n",
                    "{end_lbl}:\n"
                ),
                lhs = generate_expression(context, &*lhs),
                rhs = generate_expression(context, &*rhs),
                rhs_lbl = context.generate_label(),
                end_lbl = context.generate_label(),
	    ),
	Expr::BinOp(BinOp::Or, lhs, rhs) =>
	    format!(
                concat!(
                    "{lhs}\n",
                    "cmpl $0, %eax\n",
                    "je {rhs_lbl}\n",
                    "jmp {end_lbl}\n",
                    "{rhs_lbl}:\n",
                    "{rhs}\n",
                    "cmpl $0, %eax\n",
                    "mov $0, %eax\n",
                    "setne %al\n",
                    "{end_lbl}:\n"
                ),
                lhs = generate_expression(context, &*lhs),
                rhs = generate_expression(context, &*rhs),
                rhs_lbl = context.generate_label(),
                end_lbl = context.generate_label(),
	    ),
        Expr::BinOp(op, lhs, rhs) => {
            format!(
                concat!("{lhs}\n",
			"push %rax\n",
			"{rhs}\n",
			"{op}",),
                lhs = generate_expression(context, &*lhs),
                rhs = generate_expression(context, &*rhs),
                op = match op {
                    BinOp::Add => "pop %rcx\naddl %ecx, %eax\n",
                    BinOp::Mul => "pop %rcx\nimul %ecx, %eax\n",
                    BinOp::Sub => concat!(
			"movl %eax, %ecx\n",
			"pop %rax\n",
			"subl %ecx, %eax\n"
		    ),
		    BinOp::Div => concat!(
			"mov %eax, %ecx\n",
			"pop %rax\n",
			"cdq\n",
			"idiv %ecx\n"
		    ),
		    BinOp::Mod => concat!(
			"mov %eax, %ecx\n",
			"pop %rax\n",
			"cdq\n",
			"idiv %ecx\n",
			"mov %edx, %eax",
		    ),
		    BinOp::Eq => concat!(
			"pop %rcx\n",
			"cmpl %eax, %ecx\n",
			"mov $0, %eax\n",
			"sete %al\n"
		    ),
		    BinOp::Neq => concat!(
			"pop %rcx\n",
			"cmpl %eax, %ecx\n",
			"mov $0, %eax\n",
			"setne %al\n"
		    ),
		    BinOp::Leq => concat!(
			"pop %rcx\n",
			"cmpl %eax, %ecx\n",
			"mov $0, %eax\n",
			"setle %al\n"
		    ),
		    BinOp::Lt => concat!(
			"pop %rcx\n",
			"cmpl %eax, %ecx\n",
			"mov $0, %eax\n",
			"setl %al\n"
		    ),
		    BinOp::Gt => concat!(
			"pop %rcx\n",
			"cmpl %eax, %ecx\n",
			"mov $0, %eax\n",
			"setg %al\n"
		    ),
		    BinOp::Geq => concat!(
			"pop %rcx\n",
			"cmpl %eax, %ecx\n",
			"mov $0, %eax\n",
			"setge %al\n"
		    ),
		    BinOp::BitAnd => "pop %rcx\nandl %ecx, %eax\n",
		    BinOp::BitOr =>  "pop %rcx\norl %ecx, %eax\n",
		    BinOp::BitXor => "pop %rcx\nxorl %ecx, %eax\n",
		    BinOp::Shl => concat!(
			"movl %eax, %ecx\n",
			"pop %rax\n",
			"sall %ecx, %eax\n"
		    ),
		    BinOp::Shr => concat!(
			"movl %eax, %ecx\n",
			"pop %rax\n",
			"sarl %ecx, %eax\n"
		    ),
		    BinOp::And | BinOp::Or => unreachable!("covered before"),
                }
            )
        }
        Expr::Var(var) => format!("movq {}(%rbp), %rax\n", context.get_var_offset(var)),
    }
}
