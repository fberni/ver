use lazy_static::lazy_static;
use regex::Regex;

use std::fmt;
use std::fs;

// TODO: use functions rather than macros to check for matches

#[macro_export]
macro_rules! token_matches {
    ( $expression: expr, $( $pattern: pat_param ) |+ $( if $guard: expr )? $(,)? ) => {
        {
            match $expression {
		Some(token) => matches!(&token.tok_type, $( $pattern ) |+ $( if $guard )?),
		None => false,
	    }
        }
    }
}

// TODO: modify token_expect to print both expected and got values

#[macro_export]
macro_rules! token_expect {
    ( $expression: expr, $( $pattern: pat_param ) |+ $( if $guard: expr )? $(,)? ) => {
        {
	    let token = $expression;
	    if !matches!(token.tok_type, $( $pattern ) |+ $( if $guard )?) {
                panic!("{}: ERROR: unexpected `{}`", token.loc, token.tok_type)
            }
	    token
        }
    }
}

pub(crate) use {token_expect, token_matches};

// TODO: get rid of regexes and do manual lexing
const PATTERN_COUNT: usize = 27;
const TOKEN_PATTERNS: [&'static str; PATTERN_COUNT] = [
    r"^[a-zA-Z]\w*",
    r"^[0-9]+",
    r"^\->",
    r"^\{",
    r"^}",
    r"^\(",
    r"^\)",
    r"^;",
    r"^\-",
    r"^~",
    r"^\+",
    r"^\*",
    r"^/",
    r"^==",
    r"^!=",
    r"^<<",
    r"^>>",
    r"^<=",
    r"^<",
    r"^>=",
    r"^>",
    r"^:",
    r"^=",
    r"^%",
    r"^&",
    r"^\|",
    r"^\^",
];

// TODO: use an immutable data structure to hold tokens
#[derive(Debug)]
pub struct Lexer {
    tokens: Vec<Token>,
}

impl Lexer {
    pub fn lex(filename: &str) -> Lexer {
        lazy_static! {
            static ref REGEXES: [Regex; PATTERN_COUNT] =
                TOKEN_PATTERNS.map(|s| Regex::new(s).unwrap());
        }

        let file = fs::read_to_string(filename)
            .unwrap_or_else(|err| panic!("ERROR: could not open {}: {}", filename, err));

        let mut tokens = Vec::new();

        for (line_num, line) in file.lines().enumerate() {
            let mut column = 0;
            while column < line.len() {
                while let Some(c) = line[column..].chars().nth(0) {
                    if c == ' ' || c == '\t' {
                        column += 1
                    } else {
                        break;
                    }
                }

                if column >= line.len() {
                    break;
                }
                let (tok_type, tok_length) = if let Some(m) = REGEXES[0].find(&line[column..]) {
                    let tok_type = match m.as_str() {
                        "func" => TokenType::KwFunc,
                        "return" => TokenType::KwReturn,
                        "i32" => TokenType::KwI32,
                        "not" => TokenType::KwNot,
                        "and" => TokenType::KwAnd,
                        "or" => TokenType::KwOr,
                        "let" => TokenType::KwLet,
                        id => TokenType::Ident(String::from(id)),
                    };
                    (tok_type, m.end())
                } else if let Some(m) = REGEXES[1].find(&line[column..]) {
                    (TokenType::IntLit(String::from(m.as_str())), m.end())
                } else if let Some(m) = REGEXES[2].find(&line[column..]) {
                    (TokenType::Arrow, m.end())
                } else if let Some(m) = REGEXES[3].find(&line[column..]) {
                    (TokenType::OpenBrace, m.end())
                } else if let Some(m) = REGEXES[4].find(&line[column..]) {
                    (TokenType::CloseBrace, m.end())
                } else if let Some(m) = REGEXES[5].find(&line[column..]) {
                    (TokenType::OpenParen, m.end())
                } else if let Some(m) = REGEXES[6].find(&line[column..]) {
                    (TokenType::CloseParen, m.end())
                } else if let Some(m) = REGEXES[7].find(&line[column..]) {
                    (TokenType::Semicolon, m.end())
                } else if let Some(m) = REGEXES[8].find(&line[column..]) {
                    (TokenType::Neg, m.end())
                } else if let Some(m) = REGEXES[9].find(&line[column..]) {
                    (TokenType::BitComp, m.end())
                } else if let Some(m) = REGEXES[10].find(&line[column..]) {
                    (TokenType::Add, m.end())
                } else if let Some(m) = REGEXES[11].find(&line[column..]) {
                    (TokenType::Mul, m.end())
                } else if let Some(m) = REGEXES[12].find(&line[column..]) {
                    (TokenType::Div, m.end())
                } else if let Some(m) = REGEXES[13].find(&line[column..]) {
                    (TokenType::Eq, m.end())
                } else if let Some(m) = REGEXES[14].find(&line[column..]) {
                    (TokenType::Neq, m.end())
                } else if let Some(m) = REGEXES[15].find(&line[column..]) {
                    (TokenType::Shl, m.end())
                } else if let Some(m) = REGEXES[16].find(&line[column..]) {
                    (TokenType::Shr, m.end())
                } else if let Some(m) = REGEXES[17].find(&line[column..]) {
                    (TokenType::Leq, m.end())
                } else if let Some(m) = REGEXES[18].find(&line[column..]) {
                    (TokenType::Lt, m.end())
                } else if let Some(m) = REGEXES[19].find(&line[column..]) {
                    (TokenType::Geq, m.end())
                } else if let Some(m) = REGEXES[20].find(&line[column..]) {
                    (TokenType::Gt, m.end())
                } else if let Some(m) = REGEXES[21].find(&line[column..]) {
                    (TokenType::Colon, m.end())
                } else if let Some(m) = REGEXES[22].find(&line[column..]) {
                    (TokenType::Assign, m.end())
                } else if let Some(m) = REGEXES[23].find(&line[column..]) {
                    (TokenType::Mod, m.end())
                } else if let Some(m) = REGEXES[24].find(&line[column..]) {
                    (TokenType::BitAnd, m.end())
                } else if let Some(m) = REGEXES[25].find(&line[column..]) {
                    (TokenType::BitOr, m.end())
                } else if let Some(m) = REGEXES[26].find(&line[column..]) {
                    (TokenType::BitXor, m.end())
                } else {
                    panic!(
                        "{}: ERROR: unrecognized token\n{}",
                        FileLocation::new(filename, line_num + 1, column + 1),
                        &line
                    )
                };
                tokens.push(Token {
                    loc: FileLocation::new(filename, line_num + 1, column + 1),
                    tok_type,
                });
                column += tok_length;
            }
        }

        Lexer { tokens }
    }

    pub fn peek_token(&self) -> Option<&Token> {
        self.tokens.get(0)
    }

    pub fn peek_nth(&self, n: usize) -> Option<&Token> {
        self.tokens.get(n)
    }

    pub fn pop_token(&mut self) -> Token {
        match self.peek_token() {
            Some(_) => self.tokens.remove(0),
            None => panic!("ERROR: expected token; got end of file"),
        }
    }
}

#[derive(Debug)]
pub struct FileLocation {
    file: String,
    line: usize,
    col: usize,
}

impl FileLocation {
    pub fn new(file: &str, line: usize, col: usize) -> FileLocation {
        FileLocation {
            file: String::from(file),
            line,
            col,
        }
    }
}

impl fmt::Display for FileLocation {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}:{}:{}", self.file, self.line, self.col)
    }
}

#[derive(Debug)]
pub struct Token {
    pub loc: FileLocation,
    pub tok_type: TokenType,
}

#[derive(Debug)]
pub enum TokenType {
    KwFunc,
    KwReturn,
    KwI32,
    KwNot,
    KwAnd,
    KwOr,
    KwLet,
    Ident(String),
    IntLit(String),
    Arrow,
    OpenBrace,
    CloseBrace,
    OpenParen,
    CloseParen,
    Semicolon,
    Neg,
    BitComp,
    BitAnd,
    BitOr,
    BitXor,
    Shl,
    Shr,
    Add,
    Mul,
    Div,
    Mod,
    Eq,
    Neq,
    Leq,
    Lt,
    Geq,
    Gt,
    Colon,
    Assign,
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                TokenType::KwFunc => "func",
                TokenType::KwReturn => "return",
                TokenType::KwI32 => "i32",
                TokenType::KwNot => "not",
                TokenType::KwAnd => "and",
                TokenType::KwOr => "or",
                TokenType::KwLet => "let",
                TokenType::Ident(id) => id,
                TokenType::IntLit(lit) => lit,
                TokenType::Arrow => "->",
                TokenType::OpenBrace => "{{",
                TokenType::CloseBrace => "}}",
                TokenType::OpenParen => "(",
                TokenType::CloseParen => ")",
                TokenType::Semicolon => ";",
                TokenType::Neg => "-",
                TokenType::BitComp => "~",
                TokenType::BitAnd => "&",
                TokenType::BitOr => "|",
                TokenType::BitXor => "^",
                TokenType::Shl => "<<",
                TokenType::Shr => ">>",
                TokenType::Add => "+",
                TokenType::Mul => "*",
                TokenType::Div => "/",
		TokenType::Mod => "%",
                TokenType::Eq => "==",
                TokenType::Neq => "!=",
                TokenType::Leq => "<=",
                TokenType::Lt => "<",
                TokenType::Geq => ">=",
                TokenType::Gt => ">",
                TokenType::Colon => ":",
                TokenType::Assign => "=",
            }
        )
    }
}
