use std::fmt;

use crate::lexer::*;

// TODO: add FileLocation to each AST node

#[derive(Debug)]
pub enum UnOp {
    BitComp,
    Neg,
    Not,
}

#[derive(Debug)]
pub enum BinOp {
    Add,
    Sub,
    Mul,
    Div,
    Mod,
    BitOr,
    BitAnd,
    BitXor,
    Shl,
    Shr,
    And,
    Or,
    Eq,
    Neq,
    Leq,
    Lt,
    Geq,
    Gt,
}

#[derive(Debug)]
pub enum Expr {
    Constant(i32),
    UnOp(UnOp, Box<Expr>),
    BinOp(BinOp, Box<Expr>, Box<Expr>),
    Var(String),
}

// TODO: should parse functions be methods or not?
// TODO: most of these functions can be simplified
impl Expr {
    pub fn parse(lexer: &mut Lexer) -> Expr {
        let mut expr = Expr::parse_and_expr(lexer);
        while token_matches!(lexer.peek_token(), TokenType::KwOr) {
            let token = lexer.pop_token();
            expr = match token.tok_type {
                TokenType::KwOr => Expr::BinOp(
                    BinOp::Or,
                    Box::new(expr),
                    Box::new(Expr::parse_and_expr(lexer)),
                ),
                _ => unreachable!(),
            };
        }
        expr
    }

    pub fn parse_and_expr(lexer: &mut Lexer) -> Expr {
        let mut and_expr = Expr::parse_bit_or_expr(lexer);
        while token_matches!(lexer.peek_token(), TokenType::KwAnd) {
            let token = lexer.pop_token();
            and_expr = match token.tok_type {
                TokenType::KwAnd => Expr::BinOp(
                    BinOp::And,
                    Box::new(and_expr),
                    Box::new(Expr::parse_bit_or_expr(lexer)),
                ),
                _ => unreachable!(),
            }
        }
        and_expr
    }

    pub fn parse_bit_or_expr(lexer: &mut Lexer) -> Expr {
        let mut or_expr = Expr::parse_bit_xor_expr(lexer);
        while token_matches!(lexer.peek_token(), TokenType::BitOr) {
            let token = lexer.pop_token();
            or_expr = match token.tok_type {
                TokenType::BitOr => Expr::BinOp(
                    BinOp::BitOr,
                    Box::new(or_expr),
                    Box::new(Expr::parse_bit_xor_expr(lexer)),
                ),
                _ => unreachable!(),
            }
        }
        or_expr
    }

    pub fn parse_bit_xor_expr(lexer: &mut Lexer) -> Expr {
	let mut xor_expr = Expr::parse_bit_and_expr(lexer);
        while token_matches!(lexer.peek_token(), TokenType::BitXor) {
            let token = lexer.pop_token();
            xor_expr = match token.tok_type {
                TokenType::BitXor => Expr::BinOp(
                    BinOp::BitXor,
                    Box::new(xor_expr),
                    Box::new(Expr::parse_bit_and_expr(lexer)),
                ),
                _ => unreachable!(),
            }
        }
        xor_expr
    }

    pub fn parse_bit_and_expr(lexer: &mut Lexer) -> Expr {
	let mut and_expr = Expr::parse_eq_expr(lexer);
        while token_matches!(lexer.peek_token(), TokenType::BitAnd) {
            let token = lexer.pop_token();
            and_expr = match token.tok_type {
                TokenType::BitAnd => Expr::BinOp(
                    BinOp::BitAnd,
                    Box::new(and_expr),
                    Box::new(Expr::parse_eq_expr(lexer)),
                ),
                _ => unreachable!(),
            }
        }
        and_expr
    }

    pub fn parse_eq_expr(lexer: &mut Lexer) -> Expr {
        let mut eq_expr = Expr::parse_rel_expr(lexer);
        while token_matches!(lexer.peek_token(), TokenType::Eq | TokenType::Neq) {
            let token = lexer.pop_token();
            eq_expr = match token.tok_type {
                TokenType::Eq => Expr::BinOp(
                    BinOp::Eq,
                    Box::new(eq_expr),
                    Box::new(Expr::parse_rel_expr(lexer)),
                ),
                TokenType::Neq => Expr::BinOp(
                    BinOp::Neq,
                    Box::new(eq_expr),
                    Box::new(Expr::parse_rel_expr(lexer)),
                ),
                _ => unreachable!(),
            }
        }
        eq_expr
    }

    pub fn parse_rel_expr(lexer: &mut Lexer) -> Expr {
        let mut rel_expr = Expr::parse_shift_expr(lexer);
        while token_matches!(
            lexer.peek_token(),
            TokenType::Lt | TokenType::Leq | TokenType::Gt | TokenType::Geq
        ) {
            let token = lexer.pop_token();
            rel_expr = match token.tok_type {
                TokenType::Lt => Expr::BinOp(
                    BinOp::Lt,
                    Box::new(rel_expr),
                    Box::new(Expr::parse_shift_expr(lexer)),
                ),
                TokenType::Leq => Expr::BinOp(
                    BinOp::Leq,
                    Box::new(rel_expr),
                    Box::new(Expr::parse_shift_expr(lexer)),
                ),
                TokenType::Gt => Expr::BinOp(
                    BinOp::Gt,
                    Box::new(rel_expr),
                    Box::new(Expr::parse_shift_expr(lexer)),
                ),
                TokenType::Geq => Expr::BinOp(
                    BinOp::Geq,
                    Box::new(rel_expr),
                    Box::new(Expr::parse_shift_expr(lexer)),
                ),
                _ => unreachable!(),
            }
        }
        rel_expr
    }

    pub fn parse_shift_expr(lexer: &mut Lexer) -> Expr {
	let mut shift_expr = Expr::parse_additive_expr(lexer);
        while token_matches!(lexer.peek_token(), TokenType::Shl | TokenType::Shr) {
            let token = lexer.pop_token();
            shift_expr = Expr::BinOp(
		match token.tok_type {
		    TokenType::Shl => BinOp::Shl,
		    TokenType::Shr => BinOp::Shr,
		    _ => unreachable!(),
		},
		Box::new(shift_expr),
		Box::new(Expr::parse_additive_expr(lexer)));
	}
        shift_expr
    }

    pub fn parse_additive_expr(lexer: &mut Lexer) -> Expr {
        let mut additive_expr = Expr::parse_term(lexer);
        while token_matches!(lexer.peek_token(),TokenType::Add | TokenType::Neg) {
            let token = lexer.pop_token();
            additive_expr = match token.tok_type {
                TokenType::Add => Expr::BinOp(
                    BinOp::Add,
                    Box::new(additive_expr),
                    Box::new(Expr::parse_term(lexer)),
                ),
                TokenType::Neg => Expr::BinOp(
                    BinOp::Sub,
                    Box::new(additive_expr),
                    Box::new(Expr::parse_term(lexer)),
                ),
                _ => unreachable!(),
            }
        }
        additive_expr
    }

    fn parse_term(lexer: &mut Lexer) -> Expr {
        let mut factor = Expr::parse_factor(lexer);
        while token_matches!(
            lexer.peek_token(),
            TokenType::Mul | TokenType::Div | TokenType::Mod
        ) {
            let token = lexer.pop_token();
            factor = match token.tok_type {
                TokenType::Mul => Expr::BinOp(
                    BinOp::Mul,
                    Box::new(factor),
                    Box::new(Expr::parse_factor(lexer)),
                ),
                TokenType::Div => Expr::BinOp(
                    BinOp::Div,
                    Box::new(factor),
                    Box::new(Expr::parse_factor(lexer)),
                ),
		TokenType::Mod => Expr::BinOp(
                    BinOp::Mod,
                    Box::new(factor),
                    Box::new(Expr::parse_factor(lexer)),
                ),
                _ => unreachable!(),
            }
        }
        factor
    }

    fn parse_factor(lexer: &mut Lexer) -> Expr {
        let token = lexer.pop_token();
        match token.tok_type {
            TokenType::OpenParen => {
                let expr = Expr::parse(lexer);
                token_expect!(lexer.pop_token(), TokenType::CloseParen);
                expr
            }
            TokenType::Neg => {
                let expr = Expr::parse_factor(lexer);
                Expr::UnOp(UnOp::Neg, Box::new(expr))
            }
            TokenType::BitComp => {
                let expr = Expr::parse_factor(lexer);
                Expr::UnOp(UnOp::BitComp, Box::new(expr))
            }
            TokenType::KwNot => {
                let expr = Expr::parse_factor(lexer);
                Expr::UnOp(UnOp::Not, Box::new(expr))
            }
            TokenType::IntLit(lit) => match lit.parse::<i32>() {
                Ok(value) => Expr::Constant(value),
                Err(err) => panic!(
                    "{}: ERROR: invalid integer literal `{}`\n{}",
                    token.loc, lit, err
                ),
            },
            TokenType::Ident(var) => Expr::Var(var),
            tok_type => panic!(
                "{}: ERROR: unexpected `{}` in expression",
                token.loc, tok_type
            ),
        }
    }
}

// TODO: change the display method to something prettier
impl fmt::Display for Expr {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Expr::UnOp(UnOp::BitComp, expr) => {
                write!(f, "COMPLEMENT ({})", expr)
            }
            Expr::UnOp(UnOp::Neg, expr) => write!(f, "NEGATIVE ({})", expr),
            Expr::UnOp(UnOp::Not, expr) => {
                write!(f, "NOT ({})", expr)
            }
            Expr::BinOp(BinOp::Add, lhs, rhs) => {
                write!(f, "({}) PLUS ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Sub, lhs, rhs) => {
                write!(f, "({}) MINUS ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Mul, lhs, rhs) => {
                write!(f, "({}) MULTIPLY ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Div, lhs, rhs) => {
                write!(f, "({}) DIVIDE ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Mod, lhs, rhs) => {
                write!(f, "({}) MOD ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::BitAnd, lhs, rhs) => {
                write!(f, "({}) BAND ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::BitOr, lhs, rhs) => {
                write!(f, "({}) BOR ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::BitXor, lhs, rhs) => {
                write!(f, "({}) BXOR ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Shl, lhs, rhs) => {
                write!(f, "({}) SHL ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Shr, lhs, rhs) => {
                write!(f, "({}) SHR ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::And, lhs, rhs) => {
                write!(f, "({}) AND ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Or, lhs, rhs) => {
                write!(f, "({}) OR ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Eq, lhs, rhs) => {
                write!(f, "({}) EQ ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Neq, lhs, rhs) => {
                write!(f, "({}) NEQ ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Lt, lhs, rhs) => {
                write!(f, "({}) LT ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Leq, lhs, rhs) => {
                write!(f, "({}) LTE ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Gt, lhs, rhs) => {
                write!(f, "({}) GT ({})", lhs, rhs)
            }
            Expr::BinOp(BinOp::Geq, lhs, rhs) => {
                write!(f, "({}) GTE ({})", lhs, rhs)
            }
            Expr::Constant(val) => write!(f, "i32({})", val),
            Expr::Var(var) => write!(f, "{}", var),
        }
    }
}

#[derive(Debug)]
pub enum Stmt {
    Return(Expr),
    VarDecl(String, Option<Expr>),
    VarAssign(String, Expr),
    Expr(Expr),
}

impl Stmt {
    pub fn parse(lexer: &mut Lexer) -> Stmt {
        if token_matches!(lexer.peek_token(), TokenType::KwReturn | TokenType::KwLet) {
            match lexer.pop_token().tok_type {
                TokenType::KwReturn => {
                    let expression = Expr::parse(lexer);
                    token_expect!(lexer.pop_token(), TokenType::Semicolon);
                    Stmt::Return(expression)
                }
                TokenType::KwLet => {
                    let var = token_expect!(lexer.pop_token(), TokenType::Ident(_));
                    let var = match var.tok_type {
                        TokenType::Ident(id) => id,
                        _ => unreachable!(),
                    };
                    token_expect!(lexer.pop_token(), TokenType::Colon);
                    token_expect!(lexer.pop_token(), TokenType::KwI32);
                    let expr = if token_matches!(lexer.peek_token(), TokenType::Assign) {
                        lexer.pop_token();
                        Some(Expr::parse(lexer))
                    } else {
                        None
                    };
                    token_expect!(lexer.pop_token(), TokenType::Semicolon);
                    Stmt::VarDecl(var, expr)
                }
                _ => unreachable!(),
            }
        } else {
            if token_matches!(lexer.peek_token(), TokenType::Ident(_))
                && token_matches!(lexer.peek_nth(1), TokenType::Assign)
            {
                if let TokenType::Ident(id) = lexer.pop_token().tok_type {
                    lexer.pop_token(); // We know it's `=`, no need to check
                    let expr = Expr::parse(lexer);
                    token_expect!(lexer.pop_token(), TokenType::Semicolon);
                    Stmt::VarAssign(id, expr)
                } else {
                    unreachable!()
                }
            } else {
                let expr = Expr::parse(lexer);
                token_expect!(lexer.pop_token(), TokenType::Semicolon);
                Stmt::Expr(expr)
            }
        }
    }
}

impl fmt::Display for Stmt {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Stmt::Return(expr) => write!(f, "RETURN {}", expr),
            Stmt::Expr(expr) => write!(f, "{}", expr),
            Stmt::VarDecl(var, Some(expr)) => write!(f, "VARDECL {} = ({})", var, expr),
            Stmt::VarDecl(var, None) => write!(f, "VARDECL {}", var),
            Stmt::VarAssign(var, expr) => write!(f, "VARASSIGN {} = ({})", var, expr),
        }
    }
}

#[derive(Debug)]
pub struct FnDecl {
    pub name: String,
    pub stmts: Vec<Stmt>,
}

impl FnDecl {
    pub fn parse(lexer: &mut Lexer) -> FnDecl {
        token_expect!(lexer.pop_token(), TokenType::KwFunc);

        let token = lexer.pop_token();
        let name = match token.tok_type {
            TokenType::Ident(id) => id,
            tok_type => panic!(
                "{}: ERROR: expected identifier; got `{}`",
                token.loc, tok_type
            ),
        };

        token_expect!(lexer.pop_token(), TokenType::OpenParen);
        token_expect!(lexer.pop_token(), TokenType::CloseParen);
        token_expect!(lexer.pop_token(), TokenType::Arrow);
        token_expect!(lexer.pop_token(), TokenType::KwI32);
        token_expect!(lexer.pop_token(), TokenType::OpenBrace);

        let mut stmts = Vec::new();
        while !token_matches!(lexer.peek_token(), TokenType::CloseBrace) {
            stmts.push(Stmt::parse(lexer));
        }

        token_expect!(lexer.pop_token(), TokenType::CloseBrace);
        FnDecl { name, stmts }
    }
}

impl fmt::Display for FnDecl {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "FUNCTION {}\n\tparameters: ()\n\treturn type: i32\n\tbody:",
            self.name
        )?;
        for stmt in &self.stmts {
            write!(f, "\n\t\t{}", stmt)?;
        }

        Ok(())
    }
}

#[derive(Debug)]
pub struct Program {
    pub function: FnDecl,
}

impl Program {
    pub fn parse(lexer: &mut Lexer) -> Program {
        let function = FnDecl::parse(lexer);
        if let Some(token) = lexer.peek_token() {
            panic!("{}: ERROR: unexpected token: {}", token.loc, token.tok_type)
        }
        Program { function }
    }
}

impl fmt::Display for Program {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.function)
    }
}
