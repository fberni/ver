mod codegen;
mod context;
mod lexer;
mod parser;

use std::env;
use std::error::Error;
use std::ffi::OsStr;
use std::io::{self, Write};
use std::path::Path;
use std::process::Command;

use tempfile::{self, Builder};

use codegen::*;
use lexer::*;
use parser::*;

fn main() -> Result<(), Box<dyn Error>> {
    let args = env::args().collect::<Vec<_>>();

    // TODO: proper CLI
    if args.len() < 2 {
        println!("{}", "USAGE: ver <file>");
        panic!("ERROR: no input files");
    }

    let print_ast = args.iter().find(|&s| s == "--print-ast").is_some();

    let file = Path::new(&args[1]);

    if !matches!(file.extension(), Some(ext) if ext == "ver") {
        panic!("ERROR: input file must have .ver extension")
    }

    if !file.exists() {
        println!("{}", "USAGE: ver <file>");
        panic!("ERROR: {} does not exist", file.display())
    }

    let mut lexer = Lexer::lex(file.to_str().unwrap());
    let ast = Program::parse(&mut lexer);
    if print_ast {
        println!("{}", ast);
    }
    let generated = generate(&ast);

    let tmp_dir = tempfile::tempdir()?;

    let mut asm_file = Builder::new().suffix(".s").tempfile_in(&tmp_dir)?;
    asm_file.write(generated.as_bytes()).unwrap();

    let obj_file = asm_file.path().with_extension("o");

    let output = Command::new("as")
        .args([
            asm_file.path().as_os_str(),
            OsStr::new("-o"),
            obj_file.as_os_str(),
        ])
        .output()
        .expect("could not assemble output");
    io::stdout().write_all(&output.stdout).unwrap();
    io::stderr().write_all(&output.stderr).unwrap();
    if !output.status.success() {
        panic!()
    }

    let output = Command::new("ld")
        .args([
            obj_file.as_os_str(),
            OsStr::new("-o"),
            file.file_stem().unwrap(),
        ])
        .output()
        .expect("could not link file");
    io::stdout().write_all(&output.stdout).unwrap();
    io::stderr().write_all(&output.stderr).unwrap();
    if !output.status.success() {
        panic!()
    }

    Ok(())
}
