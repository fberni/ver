#!/usr/bin/python3

import os
import subprocess

def update_output(directory, filename):
    file_path = os.path.join(directory, filename)
    file_stem, _ = os.path.splitext(filename)
    output_file = os.path.join(directory, file_stem + ".txt")
    print(f"Compiling: {file_path}")
    cmd = subprocess.run(["./ver", file_path], capture_output=True)

    # Run executable only if compilation succeeded
    if not cmd.returncode:
        print(f"Running: ./{file_stem}")
        cmd = subprocess.run([f"./{file_stem}", file_path], capture_output=True)

        print(f"Removing: ./{file_stem}")
        os.remove(f"./{file_stem}")

    encoding = "utf-8"
    stdout = cmd.stdout.decode(encoding)
    stderr = cmd.stderr.decode(encoding)

    print(f"Updating: {output_file}")
    with open(output_file, "w") as f:
        f.write(f"STDOUT={stdout}\nSTDERR={stderr}\nRETURNCODE={cmd.returncode}\n")


def update_dir(directory):
    for subdir in os.listdir(directory):
        subdir_path = os.path.join(directory, subdir)
        for filename in os.listdir(subdir_path):
            if filename.endswith(".ver"):
                update_output(subdir_path, filename)

def main():
    if input("Are you sure you want to update all test outputs? ").lower() != "y":
        print("Cancelling...")
        return

    print("Compiling: ver")
    subprocess.run(["cargo", "build"], check=True)

    update_dir("./tests/valid/")
    update_dir("./tests/invalid/")


if __name__ == "__main__":
    main()
